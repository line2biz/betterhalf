//
//  SNAppDelegate.m
//  Betterhalf
//
//  Created by Шурик on 23.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNAppDelegate.h"

#import "SNAppearance.h"
#import "SNTakePhotoController.h"
#import "SNEditController.h"
#import "SNNavigationController.h"

@implementation SNAppDelegate

#pragma mark - Application Delegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [SNAppearance customizeAppearance];
    self.photoImages = [NSMutableArray new];
    return YES;
}

#pragma mark - Public Class Methods
+ (SNAppDelegate *)sharedDelegate
{
    return (SNAppDelegate *)[[UIApplication sharedApplication] delegate];
}

#pragma mark - Saving and Restoration
//- (BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder
//{
//    return NO;
//}
//
//- (BOOL)application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder
//{
//    return NO;
//}

@end
