//
//  SNAppDelegate.h
//  Betterhalf
//
//  Created by Шурик on 23.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM (NSUInteger, SNPhotoPositionType) {
    SNPhotoPositionTypeLeft,
    SNPhotoPositionTypeRight
};

@interface SNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) NSMutableArray *photoImages;
@property (strong, nonatomic) UIWindow *window;

+ (SNAppDelegate *)sharedDelegate;

@end
