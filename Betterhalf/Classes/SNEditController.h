//
//  SNParentController.h
//  Betterhalf
//
//  Created by Шурик on 23.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SNTakePhotoController.h"
#import "SNResizeView.h"

@interface SNEditController : UIViewController <SNTakePhotoControllerDelegate>

@property (weak, nonatomic) IBOutlet SNResizeView *leftView;
@property (weak, nonatomic) IBOutlet SNResizeView *rightView;


@end
