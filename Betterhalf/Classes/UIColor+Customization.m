//
//  UIColor+Customization.m
//  Betterhalf
//
//  Created by Шурик on 29.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "UIColor+Customization.h"

@implementation UIColor (Customization)

+ (UIColor *)customBlueColor
{
    return [UIColor colorWithRed:69/255.f green:176/255.f blue:250/255.f alpha:1];
}

+ (UIColor *)customRedColor
{
    return [UIColor colorWithRed:250/255.f green:75/255.f blue:83/255.f alpha:1];
}

+ (UIColor *)customGrayColor
{
    return [UIColor colorWithRed:47/255.f green:55/255.f blue:69/255.f alpha:1];
}

@end
