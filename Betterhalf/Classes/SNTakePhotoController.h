//
//  SNTakePhotoController.h
//  Betterhalf
//
//  Created by Шурик on 29.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SNTakePhotoControllerDelegate;

@interface SNTakePhotoController : UIViewController

@property (weak, nonatomic) id<SNTakePhotoControllerDelegate> delegate;

@property (nonatomic) BOOL firstLaunch;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic) SNPhotoPositionType photoPostionType;

- (void)configureView;

@end

@protocol SNTakePhotoControllerDelegate <NSObject>

- (void)takePhotoController:(SNTakePhotoController *)takePhotoController didSelectImage:(UIImage *)image forPositin:(SNPhotoPositionType)photoPosition;

@end