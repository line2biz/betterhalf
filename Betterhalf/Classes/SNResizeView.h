//
//  SNResizeView.h
//  Betterhalf
//
//  Created by Шурик on 31.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNResizeView : UIView

@property (strong, nonatomic) UIImage *image;
@property (nonatomic) SNPhotoPositionType photoPostionType;

@property (nonatomic) BOOL resizeabale;
@property (nonatomic) BOOL rotatable;

@end
