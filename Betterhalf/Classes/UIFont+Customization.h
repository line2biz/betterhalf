//
//  UIFont+Customization.h
//  Betterhalf
//
//  Created by Шурик on 23.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (Customization)

+ (UIFont *)meriendaOneRegularWithSize:(CGFloat)size;

@end
