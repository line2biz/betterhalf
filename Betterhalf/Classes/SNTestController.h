//
//  SNTestController.h
//  Betterhalf
//
//  Created by Шурик on 31.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SNResizeView.h"

@interface SNTestController : UIViewController

@property (weak, nonatomic) IBOutlet SNResizeView *resizeView;

@end
