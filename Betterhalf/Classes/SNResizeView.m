//
//  SNResizeView.m
//  Betterhalf
//
//  Created by Шурик on 31.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNResizeView.h"

@interface SNResizeView ()

@property (strong, nonatomic) UIImageView *imageView;

@property (strong, nonatomic) UIPanGestureRecognizer *panRecognizer;
@property (strong, nonatomic) UIPinchGestureRecognizer *pinchRecognizer;
@property (strong, nonatomic) UIRotationGestureRecognizer *rotationRecognizer;

@end

@implementation SNResizeView

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.clipsToBounds = YES;
    }
    return self;
}

- (void)removeAllRecognizers
{
    [self removeGestureRecognizer:self.rotationRecognizer];
    self.rotationRecognizer = nil;
    
    [self removeGestureRecognizer:self.pinchRecognizer];
    self.pinchRecognizer = nil;
    
    [self removeGestureRecognizer:self.rotationRecognizer];
    self.rotationRecognizer = nil;
    
}

#pragma mark - Property Accessor
- (void)setResizeabale:(BOOL)resizeabale
{
    _resizeabale = resizeabale;
    _rotatable = NO;
    [self removeAllRecognizers];
    
    
    if (_resizeabale) {
        UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panDetected:)];
        [self addGestureRecognizer:panRecognizer];
        self.panRecognizer = panRecognizer;
        
        UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchDetected:)];
        [self addGestureRecognizer:pinchRecognizer];
        self.pinchRecognizer = pinchRecognizer;
        
    }
}

- (void)setRotatable:(BOOL)rotatable
{
    _rotatable = rotatable;
    _resizeabale = NO;
    [self removeAllRecognizers];
    
    if (_rotatable) {
        UIRotationGestureRecognizer *rotationRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotationDetected:)];
        [self addGestureRecognizer:rotationRecognizer];
        self.rotationRecognizer = rotationRecognizer;
        
        UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchDetected:)];
        [self addGestureRecognizer:pinchRecognizer];
        self.pinchRecognizer = pinchRecognizer;
        
    }
}


- (void)setImage:(UIImage *)image
{
//    if (_image != image) {
        _image = image;
        if (_image) {
            [self.imageView removeFromSuperview];
            
            UIImageView *imageView = [[UIImageView alloc] initWithImage:_image];
            [imageView setContentMode:UIViewContentModeScaleToFill];
            [self addSubview:imageView];
            self.imageView = imageView;
            
//            if (self.tag == SNPhotoPositionTypeLeft) {
//                NSLog(@"LEFT");
//                
//                self.imageView.center = CGPointMake(CGRectGetMaxX(self.bounds), CGRectGetMidY(self.bounds));
////                self.imageView.layer.anchorPoint = CGPointMake(1, 0.5);
//                
//                NSLog(@"frame %@", NSStringFromCGRect(self.bounds));
//                NSLog(@"center %@", NSStringFromCGPoint(imageView.center));
//                
//            } else {
//                NSLog(@"RIGHT");
//            }
            
//            self.imageView.layer.anchorPoint = (self.tag == SNPhotoPositionTypeLeft) ? CGPointMake(1, 0.5) : CGPointMake(0, 0.5);
            
            CGFloat scale = CGRectGetHeight(self.frame) / CGRectGetHeight(imageView.frame);
            self.imageView.transform = CGAffineTransformScale(self.imageView.transform, scale, scale);
            
            if (self.tag == SNPhotoPositionTypeLeft) {
                self.imageView.center = CGPointMake(CGRectGetMaxX(self.bounds), CGRectGetMidY(self.bounds));
            } else {
                self.imageView.center = CGPointMake(CGRectGetMinX(self.bounds), CGRectGetMidY(self.bounds));
            }
            
            
//            NSLog(@"frame %@", NSStringFromCGRect(imageView.bounds));
//            NSLog(@"center %@", NSStringFromCGPoint(imageView.center));
        }
//    }
}


#pragma mark - Gesture Recognizers

- (void)panDetected:(UIPanGestureRecognizer *)panRecognizer
{
    CGPoint translation = [panRecognizer translationInView:self];
    CGPoint imageViewPosition = self.imageView.center;
    imageViewPosition.x += translation.x;
    imageViewPosition.y += translation.y;
    
    self.imageView.center = imageViewPosition;
    [panRecognizer setTranslation:CGPointZero inView:self];
}

- (void)pinchDetected:(UIPinchGestureRecognizer *)pinchRecognizer
{
    CGFloat scale = pinchRecognizer.scale;
    self.imageView.transform = CGAffineTransformScale(self.imageView.transform, scale, scale);
    pinchRecognizer.scale = 1.0;
}

- (void)rotationDetected:(UIRotationGestureRecognizer *)rotationRecognizer
{
    CGFloat angle = rotationRecognizer.rotation;
    self.imageView.transform = CGAffineTransformRotate(self.imageView.transform, angle);
    rotationRecognizer.rotation = 0.0;
}

- (void)tapDetected:(UITapGestureRecognizer *)tapRecognizer
{
    [UIView animateWithDuration:0.25 animations:^{
        self.imageView.center = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
        self.imageView.transform = CGAffineTransformIdentity;
    }];
}



@end
