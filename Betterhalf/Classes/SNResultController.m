//
//  SNResultController.m
//  Betterhalf
//
//  Created by Шурик on 29.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNResultController.h"
#import "SNTakePhotoController.h"
#import "UIColor+Customization.h"

@interface SNResultController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation SNResultController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.imageView.image = self.resultImage;
    
}

- (IBAction)didTapShareButton:(id)sender
{
    NSArray *objectsToShare = @[NSLocalizedString(@"Look at my photo", nil), self.resultImage];
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
//    NSArray *excludedActivities = @[UIActivityTypePostToTwitter, UIActivityTypePostToFacebook,
//                                    UIActivityTypePostToWeibo,
//                                    UIActivityTypeMessage, UIActivityTypeMail,
//                                    UIActivityTypePrint, UIActivityTypeCopyToPasteboard,
//                                    UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll,
//                                    UIActivityTypeAddToReadingList, UIActivityTypePostToFlickr,
//                                    UIActivityTypePostToVimeo, UIActivityTypePostToTencentWeibo];
//    controller.excludedActivityTypes = excludedActivities;
    
    [self presentViewController:controller animated:YES completion:nil];
}

- (IBAction)didTapHomeButton:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
}
- (IBAction)home:(id)sender {
    
    [[[SNAppDelegate sharedDelegate] photoImages] removeAllObjects];
    UINavigationController *nav = (UINavigationController *)self.presentingViewController;
    SNTakePhotoController *controller = (SNTakePhotoController *)[nav.viewControllers firstObject];
    controller.imageView.image = nil;
    [nav popToRootViewControllerAnimated:NO];
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
