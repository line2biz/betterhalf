//
//  SNParentController.m
//  Betterhalf
//
//  Created by Шурик on 23.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNEditController.h"

#import "UIColor+Customization.h"

#import "SNNavigationController.h"
#import "SNResultController.h"

@interface SNEditController () 

@property (weak, nonatomic) IBOutlet UIButton *takeLeftPhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *takeRightPhotoButton;

@property (weak, nonatomic) IBOutlet UIButton *editLeftPhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *editRightPhotoButton;



@end

@implementation SNEditController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.barTintColor = [UIColor customGrayColor];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStyleBordered target:nil action:nil];
    
    
    NSMutableArray *array = [[SNAppDelegate sharedDelegate] photoImages];
    
//    if (![array count]) {
//        [array addObject:[UIImage imageNamed:@"Untitled-1"]];
//        [array addObject:[UIImage imageNamed:@"Untitled-2"]];
//    }
    
    self.leftView.image = [[array firstObject] copy];
    self.rightView.image = [[array lastObject] copy];
    
    
}


#pragma mark - Navigation
- (IBAction)exitToEditController:(UIStoryboardSegue *)sender
{
    
//    NSLog(@"%s %@", __PRETTY_FUNCTION__, [[[sender sourceViewController] imageView] image]);
//    NSLog(@"%s %@", __PRETTY_FUNCTION__, [sender sourceViewController]);
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([self.editLeftPhotoButton isSelected] || [self.editRightPhotoButton isSelected]) {
        
        self.takeLeftPhotoButton.selected = NO;
        self.takeLeftPhotoButton.hidden = NO;
        
        self.editLeftPhotoButton.selected = NO;
        self.editLeftPhotoButton.hidden  = NO;
        
        [self.takeLeftPhotoButton setImage:[UIImage imageNamed:@"Icon-Photo"] forState:UIControlStateNormal];
        [self.editLeftPhotoButton setImage:[UIImage imageNamed:@"Icon-Edit"] forState:UIControlStateNormal];
        
        self.leftView.rotatable = NO;
        
        self.takeRightPhotoButton.selected = NO;
        self.takeRightPhotoButton.hidden = NO;
        
        self.editRightPhotoButton.selected = NO;
        self.editRightPhotoButton.hidden  = NO;
        
        [self.takeRightPhotoButton setImage:[UIImage imageNamed:@"Icon-Photo"] forState:UIControlStateNormal];
        [self.editRightPhotoButton setImage:[UIImage imageNamed:@"Icon-Edit"] forState:UIControlStateNormal];
        
        self.rightView.rotatable = NO;
        
        return NO;
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue destinationViewController] isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navController = (UINavigationController *)[segue destinationViewController];
        if ([[navController topViewController] isKindOfClass:[SNTakePhotoController class]]) {
            SNTakePhotoController *takePhotoController = (SNTakePhotoController *)navController.topViewController;

            UIImage *image = nil;
            SNPhotoPositionType postionType;
            
            if (sender == self.takeLeftPhotoButton) {
                postionType = SNPhotoPositionTypeLeft;
                image = self.leftView.image;
            } else {
                postionType = SNPhotoPositionTypeRight;
                image = self.rightView.image;
            }
            
            takePhotoController.delegate = self;
            takePhotoController.photoPostionType = postionType;
            takePhotoController.imageView.image = image;
            [takePhotoController configureView];
        }
        
    }
    else if ([[segue destinationViewController] isKindOfClass:[SNResultController class]]) {
        
        UIImage *leftScreenshot = [self imageFromScrollView:self.leftView];
        UIImage *rigthScrrenshot = [self imageFromScrollView:self.rightView];
        UIImage *screenshot = [self imageByCombiningImage:leftScreenshot withImage:rigthScrrenshot];
        [[segue destinationViewController] setResultImage:screenshot];
    }
}

- (UIImage *)imageFromScrollView:(SNResizeView *)theScrollView
{
    
    
//    float theScale = 1.0f / theScrollView.zoomScale;
//    // The viewing rectangle in absolute coordinates
//    CGRect visibleArea = CGRectMake((int)(theScrollView.contentOffset.x * theScale), (int)(theScrollView.contentOffset.y * theScale),
//                                    (int)(theScrollView.bounds.size.width * theScale), (int)(theScrollView.bounds.size.height * theScale));
    
    
//    UIGraphicsBeginImageContext(visibleArea.size);
//    
//    
//    
//    UIImageView *layer = [[UIImageView alloc] initWithImage:theScrollView.zoomView.image];
//    layer.frame = theScrollView.zoomView.frame;
//    
//    CALayer *coreLayer = layer.layer;
//    coreLayer.bounds = CGRectMake(layer.frame.origin.x - visibleArea.origin.x, layer.frame.origin.y - visibleArea.origin.y, layer.frame.size.width, layer.frame.size.height);
//    [coreLayer renderInContext:UIGraphicsGetCurrentContext()];
//    
//    UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContext(theScrollView.bounds.size);
    [theScrollView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return screenshot;
}

- (UIImage*)imageByCombiningImage:(UIImage*)firstImage withImage:(UIImage*)secondImage {
    
    
    UIImage *image = nil;
    
    NSLog(@"%s firstImage: %@", __FUNCTION__, NSStringFromCGSize(firstImage.size));
    
    CGSize newImageSize = CGSizeMake(firstImage.size.width + secondImage.size.width, MAX(firstImage.size.height, secondImage.size.height));
    
    if (UIGraphicsBeginImageContextWithOptions != NULL) {
        UIGraphicsBeginImageContextWithOptions(newImageSize, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(newImageSize);
    }
    
    CGFloat x = 0;
    CGFloat y = (newImageSize.height - firstImage.size.height) / 2;
    [firstImage drawAtPoint:CGPointMake(x, y)];
    
    x = firstImage.size.width;
    y = (newImageSize.height - secondImage.size.height) / 2;
    
    [secondImage drawAtPoint:CGPointMake(x, y)];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

#pragma mark - Outlet Methods
- (IBAction)didTapPhotoButton:(UIButton *)sender
{
    if ([sender isSelected]) {
        if (sender == self.takeLeftPhotoButton) {
            self.leftView.resizeabale = !self.leftView.resizeabale;
        } else {
            self.rightView.resizeabale = !self.rightView.resizeabale;
        }
        
    } else {
        [self performSegueWithIdentifier:NSStringFromClass([SNTakePhotoController class]) sender:sender];
    }
}

- (IBAction)didTapEditButton:(UIButton *)sender
{
    if (sender == self.editLeftPhotoButton) {
        if (sender.selected) {
            self.leftView.rotatable = !self.leftView.rotatable;
            
        } else {
            self.editLeftPhotoButton.selected = YES;
            [self.editLeftPhotoButton setImage:[UIImage imageNamed:@"Icon-Roatate"] forState:UIControlStateNormal];
            
            self.takeLeftPhotoButton.selected = YES;
            [self.takeLeftPhotoButton setImage:[UIImage imageNamed:@"Icon-Resize"] forState:UIControlStateNormal];
            
            self.editRightPhotoButton.hidden = YES;
            self.takeRightPhotoButton.hidden  = YES;
    
        }
    } else {
        if (sender.selected) {
            self.rightView.rotatable = !self.rightView.rotatable;
            
        } else {
            self.editRightPhotoButton.selected = YES;
            [self.editRightPhotoButton setImage:[UIImage imageNamed:@"Icon-Roatate"] forState:UIControlStateNormal];
            
            self.takeRightPhotoButton.selected = YES;
            [self.takeRightPhotoButton setImage:[UIImage imageNamed:@"Icon-Resize"] forState:UIControlStateNormal];
            
            self.editLeftPhotoButton.hidden = YES;
            self.takeLeftPhotoButton.hidden  = YES;
            
            
            
        }

        
    }
}

- (UIImage *)imageRotatedByDegrees:(UIImage*)oldImage deg:(CGFloat)degrees{
    //Calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,oldImage.size.width, oldImage.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(degrees * M_PI / 180);
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    
    //Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    //Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    //Rotate the image context
    CGContextRotateCTM(bitmap, (degrees * M_PI / 180));
    
    //Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-oldImage.size.width / 2, -oldImage.size.height / 2, oldImage.size.width, oldImage.size.height), [oldImage CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}



#pragma mark - Take Photo Controller Delegate
- (void)takePhotoController:(SNTakePhotoController *)takePhotoController didSelectImage:(UIImage *)image forPositin:(SNPhotoPositionType)photoPosition
{
    NSMutableArray *array = [[SNAppDelegate sharedDelegate] photoImages];
    
    if (photoPosition == SNPhotoPositionTypeLeft) {
        self.leftView.image = image;
        [array replaceObjectAtIndex:0 withObject:image];
    } else {
        self.rightView.image = image;
        [array replaceObjectAtIndex:1 withObject:image];
    }
}






@end
