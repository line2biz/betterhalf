//
//  SNTestController.m
//  Betterhalf
//
//  Created by Шурик on 31.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNTestController.h"

@interface SNTestController ()

@end

@implementation SNTestController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.resizeView.image = [UIImage imageNamed:@"Untitled-3.jpg"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)resize:(id)sender
{
    self.resizeView.resizeabale = !self.resizeView.resizeabale;
}
- (IBAction)rotate:(id)sender
{
    self.resizeView.rotatable = !self.resizeView.rotatable;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
