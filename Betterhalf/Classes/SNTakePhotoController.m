//
//  SNTakePhotoController.m
//  Betterhalf
//
//  Created by Шурик on 29.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNTakePhotoController.h"

#import "UIColor+Customization.h"
#import "SNEditController.h"
#import "SNNavigationController.h"


@import AVFoundation;
#import <AssertMacros.h>

@interface SNTakePhotoController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>





@property (nonatomic) UIImagePickerController *imagePickerController;
@property (weak, nonatomic) IBOutlet UIButton *takePhotoButton;

@property (weak, nonatomic) NSTimer *cameraTimer;
@property (strong, nonatomic) UIView *overlayView;

@end

@implementation SNTakePhotoController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.hidesBackButton = YES;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Main-Bkg"]];
    self.imageView.backgroundColor = [UIColor clearColor];
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        self.takePhotoButton.enabled = NO;
    }
    
    NSArray *array = [[SNAppDelegate sharedDelegate] photoImages];
    if (self.photoPostionType == SNPhotoPositionTypeLeft) {
        self.navigationItem.title = NSLocalizedString(@"First Photo", nil);
        self.imageView.image = [array firstObject];
    } else {
        self.navigationItem.title = NSLocalizedString(@"Second Photo", nil);
        if ([array count] == 2) {
            self.imageView.image = [array lastObject];
        }
    }
    [self configureView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self configureView];
}

#pragma mark - Public Methods
- (void)configureView
{
    if (self.photoPostionType == SNPhotoPositionTypeLeft) {
        self.navigationController.navigationBar.barTintColor = [UIColor customBlueColor];
    } else {
        self.navigationController.navigationBar.barTintColor = [UIColor customRedColor];
    }
    self.navigationItem.rightBarButtonItem.enabled = (self.imageView.image != nil);
}


#pragma mark - Outlet Methods
- (IBAction)didTapTakePhotoButton:(id)sender
{
    [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
}

- (IBAction)didTapSelectImageButton:(id)sender
{
    
    
    [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (IBAction)didTapCancelButton:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)didTapSaveButton:(id)sender
{
    if (!self.delegate) {
        
        if ([self.navigationController.viewControllers firstObject] == self) {
            [[[SNAppDelegate sharedDelegate] photoImages] addObject:self.imageView.image];
            SNTakePhotoController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
            controller.photoPostionType = SNPhotoPositionTypeRight;
            [self.navigationController pushViewController:controller animated:YES];
            
        } else {
            
            [[[SNAppDelegate sharedDelegate] photoImages] addObject:self.imageView.image];
            SNEditController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNEditController class])];
            SNNavigationController *navControlle = [[SNNavigationController alloc] initWithRootViewController:controller];
            navControlle.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self.navigationController presentViewController:navControlle animated:YES completion:NULL];
            
        }
        
    } else {
        [self.delegate takePhotoController:self didSelectImage:self.imageView.image forPositin:self.photoPostionType];
        [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
    }
    
}

#pragma mark - Private Methods
- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
    if (self.imageView.isAnimating) {
        [self.imageView stopAnimating];
    }

    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.sourceType = sourceType;
    imagePickerController.delegate = self;
    imagePickerController.navigationBar.barTintColor = self.navigationController.navigationBar.barTintColor;
    

    self.imagePickerController = imagePickerController;
    [self.navigationController presentViewController:self.imagePickerController animated:YES completion:nil];
}

#pragma mark - Image Controller Delegate
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    self.imageView.image = image;
    self.navigationItem.rightBarButtonItem.enabled = YES;
    if ([self.cameraTimer isValid]) {
        return;
    }
    [self finishAndUpdate];
}

#pragma mark - Camera Methods
- (void)finishAndUpdate
{
    [self dismissViewControllerAnimated:YES completion:NULL];
    self.imagePickerController = nil;
}


@end
