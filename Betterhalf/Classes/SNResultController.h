//
//  SNResultController.h
//  Betterhalf
//
//  Created by Шурик on 29.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNResultController : UIViewController

@property (strong, nonatomic) UIImage *resultImage;

@end
