//
//  UIFont+Customization.m
//  Betterhalf
//
//  Created by Шурик on 23.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "UIFont+Customization.h"

@implementation UIFont (Customization)

+ (UIFont *)meriendaOneRegularWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"MeriendaOne-Regular" size:size];
}

@end
