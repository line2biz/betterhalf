//
//  SNAppearance.m
//  Betterhalf
//
//  Created by Шурик on 23.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNAppearance.h"

#import "UIFont+Customization.h"

@implementation SNAppearance

#pragma mark - Utitlity Functions


#pragma mark Navigation Bar


+ (void)customizeNavigationBar
{
    UINavigationBar *navBarAppearance = [UINavigationBar appearance];
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName : [UIFont meriendaOneRegularWithSize:18],
                                 NSForegroundColorAttributeName : [UIColor whiteColor]
                                 };
    [navBarAppearance setTitleTextAttributes:attributes];
    [navBarAppearance setTintColor:[UIColor whiteColor]];
    
//    [navBarAppearance setBarTintColor:[UIColor colorWithRed:47/255.f green:55/255.f blue:69/255.f alpha:1]];
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:attributes forState:UIControlStateNormal];
//    [[UIBarButtonItem appearance] setTitleTextAttributes:attributes forState:UIControlStateSelected];
    
}

#pragma mark - Customization methods
+ (void)customizeAppearance
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [SNAppearance customizeNavigationBar];
}





@end
