//
//  SNAppearance.h
//  Betterhalf
//
//  Created by Шурик on 23.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNAppearance : NSObject

+ (void)customizeAppearance;

@end
